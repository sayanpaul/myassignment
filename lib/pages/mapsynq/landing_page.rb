class RadioSetUI < SitePrism::Section
  element :incidents, :css, 'label:nth-of-type(1)'
  element :cameras, :css, 'label:nth-of-type(2)'
  element :tolls, :css, 'label:nth-of-type(3)'
end

class LeftTab < SitePrism::Section
element :tab_directions, :css, '.tab_button directions_tab sprite'
element :tab_personal, :css, '.tab_button.me_tab.sprite'
element :tab_live, :css, '.tab_button.live_tab'
end

class LandingPage < BasePage
  set_url '/'
  section :left_tab, LeftTab, :css, '.left_tab'
  section :radio_set_set_ui, RadioSetUI,:css,'.radioset.ui-buttonset'

def tab_active(element)
  element = element['class']
  element.match?(/tab_active/)
end

  def radio_active(element)
    element = element['class']
    element.match?(/ui-state-active/)
  end
end

