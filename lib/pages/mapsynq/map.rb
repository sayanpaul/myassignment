class MapButton < SitePrism::Section
  element :toll, :css, 'div:nth-of-type(1)'
  element :cameras, :css, 'div:nth-of-type(2)'
  element :parking, :css, 'div:nth-of-type(3)'
  element :incidents,:css, 'div:nth-of-type(4)'
  element :traffic,:css, 'div:nth-of-type(5)'
end

class Map < BasePage
  section :map_button, MapButton, :css,'.buttonPanel.olControlNoSelect'

  def active(element)
    element = element['class']
    element.match?(/Active/)
  end

  def inactive(element)
    element = element['class']
    element.match?(/Inactive/)
  end
end