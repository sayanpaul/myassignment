require 'active_model'

class BasePage < SitePrism::Page
  include ActiveModel::Validations
end