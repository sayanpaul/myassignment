require 'time'

Before do
  @landing_page = LandingPage.new
  @date = Date.today.strftime
  @map = Map.new
  page.driver.browser.manage.window.resize_to(1024,768 )
end

Before  do |scenario|
  begin
    puts("NO HOOK")
  rescue => e
    STDOUT.puts 'failed pre-test setup in BEFORE HOOK'
    STDOUT.puts e
    STDOUT.puts e.backtrace
    $setup_failed = true
  end
end

After do |scenario|
  take_screenshot(Capybara, scenario)
  page.execute_script "window.close();"
  # page.driver.browser.close
end


at_exit do
  ReportBuilder.configure do |config|
    config.input_path = 'test-reports.json'
    config.report_path = 'my_test_report'
    config.report_types = [:html]
    config.report_title = 'My Test Results'
    config.additional_info = {browser: ENV['BROWSER'], environment: ENV['RAILS_TEST_ENV']}
  end
  ReportBuilder.build_report
end

def take_screenshot(browser, scenario)
  unless browser.current_path.nil?
    if RUBY_PLATFORM.include?('linux')
      begin
        Dir.mkdir('screenshots') unless Dir.exist?('screenshots')
      rescue
        STDOUT.puts 'failed to create screenshots directory, may already exist'
      end
      if !scenario.passed?
      scenario_name = scenario.name.gsub /[^\w\-]/, ' '
      time = Time.now.strftime('%Y-%m-%d %H%M%S')
        STDOUT.puts 'FAILURE ' + time + '-' + scenario_name
        screenshot_path = 'screenshots/' + 'FAILURE' + time + '-' + scenario_name +  '.png'
      browser.save_screenshot(screenshot_path)
      embed(screenshot_path,"image/png")
      end
    end
  end
end
