require 'capybara/cucumber'
require 'cucumber'
require 'fileutils'
require 'rspec/expectations'
require 'selenium-webdriver'
require 'site_prism'
require 'report_builder'
require_relative 'browser'
require 'browserstack/local'

Before do
    $PROJECT_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '../'))
  require "#{$PROJECT_ROOT}/lib/pages/base_page.rb" # require base_page first so that other pages won't fail
  Dir["#{$PROJECT_ROOT}/lib/pages/*.rb"].each { |file| require file }

  Capybara.default_selector = :css
  Capybara.ignore_hidden_elements = true
  Helpers.reset_wait_time
  Capybara.run_server = false
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.timeout = 240

  if ENV['LOCAL'] == 'true'
    puts 'starting local run'
    Capybara.default_driver = ENV['BROWSER'].downcase.to_sym
    Capybara.register_driver ENV['BROWSER'].downcase.to_sym do |app|
      Capybara::Selenium::Driver.new(app, browser: ENV['BROWSER'].downcase.to_sym, profile: @profile)
    end
  else
    puts 'starting remote run'
    browser_stack_file = 'support/data/browserstack_setup.yaml'
    if File.exist?(browser_stack_file)
      puts "loading browser stack yaml #{browser_stack_file}"
      browserstack_yaml = YAML.load(File.open(browser_stack_file))
    end
    Capybara.register_driver :browserstack do |app|
      caps = Browser.remote
    @bs_local = BrowserStack::Local.new
    bs_local_args = { 'key' => browserstack_yaml['key'], 'force' => 'true' }
    @bs_local.start(bs_local_args)

    Capybara::Selenium::Driver.new(app,
                                   browser: :remote,
                                   url: "http://#{browserstack_yaml['name']}:#{browserstack_yaml['key']}@hub.browserstack.com/wd/hub",
                                   desired_capabilities: caps)

  end
  Capybara.default_driver = :browserstack
  Capybara.run_server = false

  at_exit do
    Timeout.timeout(10) do
      @bs_local.stop unless @bs_local.nil?
    end
  end

  end

  # Set the environment variable HEADFUL to fit your setup
  # True = run with full browser, anything else gets headless
  if ENV['HEADFUL'].eql?('false')
    require 'headless'
    Headless.new(display: 100, reuse: true, destroy_at_exit: false).start
  end

  STDOUT.sync = true
end


env_file = "support/environments/#{ENV['RAILS_TEST_ENV'].downcase}.yaml"
if File.exist?(env_file)
  puts "loading environment yaml #{env_file}"
  env_yaml = YAML.load(File.open(env_file))
  Capybara.app_host = env_yaml['url']
else
  puts "So you think you can run the automation in an environment that doesn't exist?"
end


module AutomationWorld
  def method_missing(method, *args, &block)
    if method =~ /_page$/
      var = "@#{method}"
      ivar = instance_variable_get(var)
      unless ivar
        page = method.to_s.classify.constantize.new
        ivar = instance_variable_set(var_name, page)
      end
      ivar
    else
      super
    end
  end
end
World(AutomationWorld)
