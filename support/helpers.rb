module Helpers

  def self.set_default_wait_time(time)
    Capybara.default_max_wait_time = time
  end

  def self.reset_wait_time
    Capybara.default_max_wait_time = ENV['CONNECT_TIMEOUT'].to_i!=0 ? ENV['CONNECT_TIMEOUT'].to_i : 60
  end

  def self.grab_screenshot(optional_text = nil)

    Dir.mkdir('screenshots') unless Dir.exists?('screenshots')

    time = Time.now.strftime('%Y-%m-%d %H%M')
    screenshot_path = 'screenshots/' + "#{optional_text} " + time + ' - ' + '.png'
    Capybara.save_screenshot(screenshot_path)
  end

  def self.wait_for_page_load
    STDOUT.puts 'Made it to wait_for_page_load'
    until Capybara.page.evaluate_script('jQuery.active') == 0 do
      sleep 1
      STDOUT.puts 'Waiting for ajax'
    end
  end

  def self.highlight(page,element, duration = 3)
    # store original style so it can be reset later
    original_style = element.native.css_value("style")

    # style element with yellow border
    page.execute_script(
        "arguments[0].setAttribute(arguments[1], arguments[2])",
        element,
        "style",
        "border: 2px solid red; border-style: dashed;")

    # keep element highlighted for a spell and then revert
    if duration > 0
      sleep duration
      page.execute_script(
          "arguments[0].setAttribute(arguments[1], arguments[2])",
          element,
          "style",
          original_style)
    end

  end

end
