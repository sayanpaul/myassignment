require 'selenium-webdriver'

class Browser
  case ENV['BROWSER'].downcase
  when "firefox"
  @profile = Selenium::WebDriver::Firefox::Profile.new
    when "chrome"
      @profile = Selenium::WebDriver::Chrome::Profile.new

  end

  @profile['timeout'] = 480000
  @profile['pdfjs.disabled'] = true
  @profile['resynchronization_timeout'] = 90
  @profile['resynchronize '] = true

  def self.remote()
  caps=Selenium::WebDriver::Remote::Capabilities.new
  caps["os"] = ENV['os']|| "Windows"
  caps["os_version"] = ENV['OS_VERSION'] || "10"
  caps["browser"] = ENV['BROWSER']||"Chrome"
  caps["browser_version"] = ENV['BROWSER_VERSION']||"62.0"
  caps["browserstack.networkLogs"] = "true"
  caps['javascriptEnabled'] = 'true'
  caps["browserstack.local"] = "true"
    return caps
    end
end

