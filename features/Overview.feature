Feature: Basic Landing Page

Scenario: Buttons default on landing page
  Given I am on the landing Page
  Then I can see Incidents button enabled on the map
  And I can see the left panel coloumn defaulted as Live
  And I can see the left is defaulted to Incidents
