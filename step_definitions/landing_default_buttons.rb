Given(/^I am on the landing Page$/) do
  @landing_page.load
end

Then(/^I can see (Traffic|Incidents|Parking|Cameras|Tolls) button enabled on the map$/) do |buttons|
case buttons.downcase
  when 'incidents'
   element = @map.map_button.incidents
end
  expect(@map.active(element)).to be true
end

And(/^I can see the left panel coloumn defaulted as (Live|Personal|Directions)$/) do |buttons|
  case buttons.downcase
    when "live"
      element =  @landing_page.left_tab.tab_live
    when "personal"
      element = @landing_page.left_tab.tab_personal
    when "directions"
      element = @landing_page.left_tab.tab_directions
  end
  expect(@landing_page.tab_active(element)).to be true
end

And(/^I can see the left is defaulted to (Incidents|Cameras|Tolls)$/) do |buttons|
  case buttons.downcase
    when "incidents"
      element =  @landing_page.radio_set_set_ui.incidents
    when "cameras"
      element = @landing_page.radio_set_set_ui.cameras
    when "tolls"
      element = @landing_page.radio_set_set_ui.tolls
  end
  expect(@landing_page.radio_active(element)).to be true

end